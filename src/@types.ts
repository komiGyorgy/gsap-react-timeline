export interface Slide {
  image?: string; //string that gets translated to the image imported into the file, or a url
  content: string[]; // separate paragraph strings, one element of the array will be a separate p
  title?: string; // if no image and title, a summary of a specific year will be displayed
  year: number;
  index: number;
  isPrevious?: boolean;
  isCurrent?: boolean;
  isNext?: boolean;
  isSecondNext?: boolean;
  isShown?: boolean;
  elemet?: HTMLDivElement | null;
}

export interface SelectorImage {
  url: string;
  isActive: boolean;
}
