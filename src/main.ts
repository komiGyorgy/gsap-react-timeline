import { createApp } from "vue";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import App from "@/App.vue";
import VueLazyLoad from "vue3-lazyload";
const app = createApp(App);

import error from "./assets/error.gif";
import loading from "./assets/loading.gif";
app.use(VueLazyLoad, {
  loading,
  error,
  log: false,
});
app.mount("#app");
