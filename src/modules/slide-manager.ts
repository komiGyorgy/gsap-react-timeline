import { Slide } from "@/@types";

type BufferIndex = number | null;
function mergeReplace(a1: Slide[], a2: Slide[]) {
  const i = a2[0].index;
  return [...a1.slice(0, i), ...a2, ...a1.slice(i + a2.length)];
}
export default class SlideManager {
  _slides: Slide[] = [];
  _slideBuffer: Slide[] = [];
  _previousIndex: BufferIndex = null;
  _currentIndex = 0;
  _nextIndex: BufferIndex = 1;
  _anteNextIndex: BufferIndex = 2;
  constructor(slides: Omit<Slide, "index">[]) {
    this._slides = slides.map((slide, index) => ({
      ...slide,
      index,
    }));
    this._buildSlideBuffer();
  }
  _changeIndex(direction: 1 | -1): boolean {
    const newIndex = this._currentIndex + direction;
    if (newIndex > this._slides.length - 1 || newIndex < 0) {
      return false;
    }
    this._currentIndex = newIndex;
    this._calculatePreviousIndex();
    this._calculateNextIndex();
    this._calculateAntedNextIndex();
    this._buildSlideBuffer();
    return true;
  }

  _calculatePreviousIndex() {
    if (this._currentIndex - 1 >= 0) {
      this._previousIndex = this._currentIndex - 1;
    } else {
      this._previousIndex = null;
    }
    console.log("_previousIndex: ", this._previousIndex);
  }
  _calculateNextIndex() {
    if (this._currentIndex + 1 > this._slides.length - 1) {
      this._nextIndex = null;
    } else {
      this._nextIndex = this._currentIndex + 1;
    }
  }
  _calculateAntedNextIndex() {
    if (this._currentIndex + 2 > this._slides.length - 1) {
      this._anteNextIndex = null;
    } else {
      this._anteNextIndex = this._currentIndex + 2;
    }
  }

  _buildSlideBuffer() {
    this._slideBuffer = Object.assign([], []);

    if (this._previousIndex !== null && this._slides[this._previousIndex]) {
      this._slideBuffer.push({
        ...this._slides[this._previousIndex],
        isPrevious: true,
        isShown: true,
      });
    }

    this._slideBuffer.push({
      ...this._slides[this._currentIndex],
      isCurrent: true,
      isShown: true,
    });

    if (this._nextIndex !== null && this._slides[this._nextIndex]) {
      this._slideBuffer.push({
        ...this._slides[this._nextIndex],
        isNext: true,
        isShown: true,
      });
    }

    if (this._anteNextIndex !== null && this._slides[this._anteNextIndex]) {
      this._slideBuffer.push({
        ...this._slides[this._anteNextIndex],
        isSecondNext: true,
        isShown: true,
      });
    }
    console.log(`this._slideBuffer`, this._slideBuffer);
  }

  rollForward(): void {
    console.log(`rollForward`, this._currentIndex + 1);
    this._changeIndex(1);
  }

  rollBackward(): void {
    console.log(`rollBackward`, this._currentIndex - 1);
    this._changeIndex(-1);
  }

  get getBuffer() {
    return this._slideBuffer;
  }

  get getUpdatedSlideData() {
    return mergeReplace(this._slides, this._slideBuffer);
  }

  get getCurrentIndex(): number {
    return this._currentIndex;
  }
  get getPreviousIndex(): BufferIndex {
    return this._previousIndex;
  }
  get getNextIndex(): BufferIndex {
    return this._nextIndex;
  }
  get getSecondNextIndex(): BufferIndex {
    return this._anteNextIndex;
  }
}
