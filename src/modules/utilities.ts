import { Slide } from "@/@types";

export function getSlideReferences({
  slide,
  index,
}: {
  slide?: Slide;
  index?: number | null;
}) {
  return index !== 0 || slide?.index !== 0
    ? {
        slideId: `#timeline-slide-${index ?? slide?.index}`,
        contentId: `#slide-content-${index ?? slide?.index}`,
      }
    : null;
}
