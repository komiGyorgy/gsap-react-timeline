type ImageType = Promise<typeof import("*.png")>;
type imageKey = keyof typeof images;

export const images = {
  elementary: async (): ImageType =>
    await import("../assets/slide_images/elementary.png"),
  graduation: async (): ImageType =>
    await import("../assets/slide_images/graduation.png"),
  jfmideas: async (): ImageType =>
    await import("../assets/slide_images/jfmideas.png"),
  talk: async (): ImageType => await import("../assets/slide_images/talk.png"),
};

export function isLegalImageName(name: imageKey | string): name is imageKey {
  return Object.hasOwnProperty.call(images, name);
}

export async function getImageFromName(
  imageName: imageKey | string
): Promise<typeof import("*.png") | string> {
  if (isLegalImageName(imageName)) {
    return await (
      await images[imageName]()
    ).default;
  }
  return imageName;
}
