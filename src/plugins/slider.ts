import { Slide } from "@/@types";
import gsap from "gsap";
export interface DataSlide extends Slide {
  element: HTMLDivElement;
  slideContent: HTMLElement | null;
  index: number;
}
export interface SlideShowOptions {
  startPaused: boolean;
  defaults: { defaults: Record<string, any> };
  smoothChildTiming: boolean;
}

const defaults = { defaults: { duration: 1, ease: "elastic" } };
type ElementReferences = { slideId: string; contentId: string };
interface AnimatorSlidePlayload {
  current: ElementReferences | null | undefined;
  next?: ElementReferences | null | undefined;
  secondNext?: ElementReferences | null | undefined;
  previous?: ElementReferences | null | undefined;
}
export class Animator {
  _timeLine: gsap.core.Timeline;
  _widthPercentage = 0.8;
  _ease = "power3.in";
  _duration = 1;
  constructor() {
    this._timeLine = gsap.timeline({ paused: true });
  }
  animateNext({ current, next, secondNext }: AnimatorSlidePlayload) {
    return new Promise((resolve) => {
      this._timeLine = gsap.timeline({
        onComplete: resolve,
        smoothChildTiming: true,
      });
      console.log("animating curr :>> ", current);
      if (current) {
        this._timeLine.to(current.slideId, {
          opacity: 0,
          duration: this._duration,
          x: (_index, target, _targets) => {
            const multiplier =
              Number(target.attributes.index.nodeValue) === 0 ? 1 : 2;
            console.log(`miltiplier`, multiplier);
            return -1 * window.innerWidth * this._widthPercentage * multiplier;
          },
          ease: this._ease,
        });
      }
      console.log("animating next :>> ", next);
      if (next) {
        this._timeLine.to(
          next.slideId,
          {
            duration: this._duration,
            x: -1 * window.innerWidth * 0.8,
            ease: this._ease,
          },
          "<"
        );
      }
      console.log("animating secondNext :>> ", secondNext);
      if (secondNext && next) {
        this._timeLine.fromTo(
          secondNext.slideId,
          {
            x: window.innerWidth * 0.8,
          },
          {
            duration: this._duration,
            x: 0,
            ease: this._ease,
          },
          "<"
        );
        this._timeLine.to(
          next.contentId,
          {
            duration: this._duration,
            paddingLeft: "200px",
            ease: "circ.inOut",
          },
          "<"
        );
      }
      this._timeLine.play();
    });
  }
  animatePrev({ previous, current, next, secondNext }: AnimatorSlidePlayload) {
    return new Promise((resolve) => {
      this._timeLine = gsap.timeline({
        onComplete: resolve,
        smoothChildTiming: true,
      });
      console.log("animating prev :>> ", previous);
      if (previous) {
        this._timeLine.fromTo(
          previous.slideId,
          {
            opacity: 0,
            x: (_index, target, _targets) => {
              const multiplier =
                Number(target.attributes.index.nodeValue) === 0 ? 1 : 2;
              console.log(`miltiplier`, multiplier);
              return (
                -1 * window.innerWidth * this._widthPercentage * multiplier
              );
            },
          },
          {
            opacity: 1,
            duration: this._duration,
            x: (_index, target, _targets) => {
              const multiplier =
                Number(target.attributes.index.nodeValue) === 0 ? 0 : 1;
              console.log(`miltiplier`, multiplier);
              return (
                -1 * window.innerWidth * this._widthPercentage * multiplier
              );
            },
            ease: this._ease,
          }
        );
      }
      console.log("animating curr :>> ", current);
      if (current) {
        this._timeLine.to(
          current.slideId,
          {
            duration: this._duration,
            x: 0,
            ease: this._ease,
          },
          "<"
        );
      }

      if (previous) {
        this._timeLine.to(
          previous.contentId,
          {
            duration: this._duration,
            paddingLeft: "200px",
            ease: "circ.inOut",
          },
          "<"
        );
      }

      console.log("animating next :>> ", next);
      if (next) {
        this._timeLine.to(
          next.slideId,
          {
            duration: this._duration,
            x: window.innerWidth * 0.8,
            ease: this._ease,
          },
          "<"
        );
      }
      console.log("animating secondNext :>> ", secondNext);
      if (secondNext) {
        this._timeLine.to(
          secondNext.slideId,
          {
            duration: this._duration,
            x: window.innerWidth * 0.8,
            ease: this._ease,
          },
          "<"
        );
      }
      if (current) {
        this._timeLine.to(
          current.contentId,
          {
            duration: this._duration,
            paddingLeft: "45px",
            ease: "circ.inOut",
          },
          "<"
        );
      }
      this._timeLine.play();
    });
  }
}
